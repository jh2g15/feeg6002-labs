#include <stdio.h>
#include <math.h>

#define N 10
#define XMIN 1
#define XMAX 10

int main (void)
{
	int i;
	float x;
	
	for(i = 0; i < N; i++)
	{
		x = XMIN + i * (XMAX - XMIN) / (float) (N - 1);
		printf("%f %f %f\n", x, sin(x), cos(x));
	}
	return 0;
}

