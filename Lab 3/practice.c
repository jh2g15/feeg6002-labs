#include <stdio.h>

struct person
{
	int age;
	double height;
	double weight;
};

void printDetails(struct person A)
{
	printf("Age is %d\n", A.age);
	printf("Height is %.1f\n", A.height);
	printf("Weight is %.1f\n", A.weight);
}

int main(void)
{
	struct person Jannic = {22, 178, 77};
	printDetails(Jannic);
	return 0;
}

