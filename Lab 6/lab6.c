#include <stdio.h>
#include <stdlib.h>

void use_fib_array(long N);
long *make_fib_array(long n);

int main(void) {
	use_fib_array(10);
	return 0;
}


void use_fib_array(long N) {
/* N is the maximum number for fibarray length */
	long n; /* counter for fibarray length */
	long i; /* counter for printing all elements of fibarray */
	long *fibarray; /* pointer to long -- pointer to the fibarray itself*/
	/* Print one line for each fibarray length n*/
	for (n=2; n<=N; n++) {
		/* Obtain an array of longs with data */
		fibarray = make_fib_array(n);
		/* Print all elements in array */
		printf("fib(%2ld) : [",n);
		for (i=0; i<n; i++) {
			printf(" %ld", fibarray[i]);
		}
		printf(" ]\n");
		/* free array memory */
		free(fibarray);
	}
}

long *make_fib_array(long n)
{
	int i;
	
	long *fib_array;
	fib_array = (long *)malloc(n * sizeof(long));
	if(fib_array == NULL)
	{
		return NULL;
	}
	
	fib_array[0] = 0;
	fib_array[1] = 1;
	
	for(i = 2; i < n; i++)
	{
		fib_array[i] = fib_array[i - 1] + fib_array[i - 2];
	}
	
	return fib_array;
}

