#include<stdio.h>

/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include<stdlib.h>
#include<math.h>
#include <time.h>

clock_t startm, stopm;

#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%8.5f seconds used .", (((double) stopm - startm)/CLOCKS_PER_SEC/RUNS));

double f(double x);
double pi(long n);

#define N 10000000

/* TIMING CODE END */
int main(void) {
	/* Declarations */
	/* Code */
	START; /* Timing measurement starts here */
	/* Code to be written by student, calling functions from here is fine
	if desired
	*/

	printf("Pi ~ %10f\n", pi(N));
	
	STOP; /* Timing measurement stops here */
	PRINTTIME; /* Print timing results */
	return 0;
}

double f(double x)
{
	return sqrt(1 - x*x);
}

double pi(long n)
{
	long i;
	float a, b, h, s, x;
	
	a = -1;
	b = 1;
	h = (b - a) / n;
	s = 0.5 * f(a) + 0.5 * f(b);
	
	for(i = 1; i < n; i++)
	{
		x = a + i * h;
		s = s + f(x);
	}
	
	return (s * h * 2);
}

