#include <stdio.h>

void rstrip(char s[]);

void rstrip(char s[]) {
	int i, j, last_char_passed;
	
	for(i = 0; s[i] != '\0'; i++)
	{
		if(s[i] == ' ')
		{
			last_char_passed = 1;
			for(j = i; s[j] != '\0'; j++)
			{
				if(s[j] != ' ')
				{
					last_char_passed = 0;
				}
			}
			if (last_char_passed == 1)
			{
				s[i] = '\0';
				break;
			}
		}
	}
}

int main(void) {
	char test1[] = "";
	printf("Original string reads : |%s|\n", test1);
	rstrip(test1);
	printf("r-stripped string reads: |%s|\n", test1);
	return 0;
}

