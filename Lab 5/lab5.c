#include <stdio.h>

void lstrip(char s[]);

void lstrip(char s[]) {
	int str_start;
	int i = 0;
	
	if(s[0] == ' ')
	{
		while(s[i] == ' ')
		{
			i++;
		}
		str_start = i;
		i = 0;
		while(s[i + str_start] != '\0')
		{
			s[i] = s[i+str_start];
			i++;
		}
		s[i] = '\0';
	}
}

int main(void) {
	char test1[] = "Hello";
	printf("Original string reads : |%s|\n", test1);
	lstrip(test1);
	printf("l-stripped string reads: |%s|\n", test1);
	return 0;
}

