#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
void reverse(char source[], char target[]);

int main(void) {
	char original[] = "20";
	char reversed[MAXLINE];
	
	printf("%s\n", original);
	reverse(original, reversed);
	printf("%s\n", reversed);
	return 0;
}

/* reverse the order of characters in 'source', write to 'target'.
Assume 'target' is big enough. */
void reverse(char source[], char target[])
{
	int i;
	int str_len;
	
	i = 0;	  
	while(source[i] != '\0')
	{
		i++;
	}
	
	str_len = i - 1;
	while(i >= 0)
	{
		target[i] = source[str_len - i];
		i--;
	}
	
	target[str_len + 1] = '\0';
}


