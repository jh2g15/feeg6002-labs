#include <stdio.h>
#include <stdlib.h>

void use_mix(void);
char *mix(char *s1, char *s2);


void use_mix(void) {
	char s1[] = "Hello";
	char s2[] = "12345";
	printf("s1 = %s\n", s1);
	printf("s2 = %s\n", s2);
	printf("r = %s\n", mix(s1, s2));
}

char *mix(char *s1, char *s2)
{
	long i, len;
	char *mix_str;
	
	len = 0;
	while (s1[len] != '\0')
	{
		len++;
	}
		
	mix_str = (char *)malloc(sizeof(char)*len*2);
	
	if(mix_str == NULL)
	{
		return NULL;
	}
	
	for(i = 0; i < 2 * len; i++)
	{
		if(i % 2 == 0)
		{
			mix_str[i] = s1[i / 2];
		}
		else
		{
			mix_str[i] = s2[i / 2];
		}
	}
	
	mix_str[len * 2] = '\0';
	
	return mix_str;
}

int main(void)
{
	use_mix();
	return 0;
}

