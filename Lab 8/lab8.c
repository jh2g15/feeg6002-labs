#include <stdio.h>

#define N 4 /*Number of people*/

struct entry {
	char firstname[20];
	char lastname[20];
	char phone[10];
};

int main(void)
{
	int i;
	struct entry list[N];
	
	for(i = 0; i < N; i++)
	{
		printf("\nEnter first name: ");
		scanf("%s",list[i].firstname);
		printf("Enter last name: ");
		scanf("%s",list[i].lastname);
		printf("Enter phone in 123-4567 format: ");
		scanf("%s",list[i].phone);
	}
	
	for(i = 0; i < N; i++)
	{
		printf("Name: %s %s \t Phone: %s\n", list[i].firstname, list[i].lastname, list[i].phone);
	}
	
	return 0;
}

