#include <stdio.h>

int main (void)
{
	int month;
	float s;
	float interest;
	float total_interest;
	float frac;
	
	s = 1000.0;
	interest = 0.0;
	total_interest = 0.0;
	frac = 0.0;
	
	for(month = 1; month <= 24; month++)
	{
		interest = 0.03 * s;
		total_interest += interest;
		frac = 100 * total_interest / (float) 1000;
		s = s * 1.03;
		printf("month %2d: debt=%7.2f, interest=%5.2f, total_interest=%6.2f, frac=%5.2f\n", month, s, interest, total_interest, frac);
	}  
	
	return 0;
}

