#include <stdio.h>

int main (void)
{
	int i;
	int T_Celsius;
	float T_Fahrenheit;
	
	T_Celsius = -30;
	
	for (i = 0; i <= 30; i++)
	{
		T_Fahrenheit = T_Celsius * (9/(float) 5) + 32;
		printf("%3d = %5.1f\n", T_Celsius, T_Fahrenheit);
		T_Celsius += 2;
	}
}

